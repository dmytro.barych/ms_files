<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Image Driver
    |--------------------------------------------------------------------------
    |
    | Intervention Image supports "GD Library" and "Imagick" to process images
    | internally. You may choose one of them according to your PHP
    | configuration. By default PHP's "GD Library" implementation is used.
    |
    | Supported: "gd", "imagick"
    |
    */

    'driver' => 'gd',

    's3_path' => env('AWS_PATH_TO_FILES'),
    'image_storage_path' => env('IMAGE_STORAGE_PATH'),
    'quality' => env('IMAGE_QUALITY'),
    'preview_w' => env('IMAGE_PREVIEW_W'),
    'preview_h' => env('IMAGE_PREVIEW_H'),
    'thumbnail_w' => env('IMAGE_THUMBNAIL_W'),
    'thumbnail_h' => env('IMAGE_THUMBNAIL_h'),
];
