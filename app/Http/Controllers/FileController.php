<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Actions\FileDeleteAction;
use App\Http\Requests\File\Index;
use App\Http\Requests\File\Store;
use App\Http\Resources\FileCollection;
use App\Http\Resources\FileResource;
use App\Models\File;
use App\Services\FileService;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class FileController extends Controller
{
    private FileService $fileService;

    public function __construct(FileService $fileService)
    {
        $this->fileService = $fileService;
    }

    public function index(Index $request): FileCollection
    {
        return new FileCollection(
            File::where('fileable_type', $request->fileable_type)
                ->where('fileable_id', $request->fileable_id)
                ->paginate(10)
        );
    }

    public function store(Store $request): FileResource
    {
        return new FileResource($this->fileService->store($request->file, $request->validated()));
    }

    public function show(File $file): FileResource
    {
        return new FileResource($file);
    }

    public function destroy(File $file, FileDeleteAction $action): JsonResponse
    {
        return response()->json(['success' => $action->handle($file)], Response::HTTP_OK);
    }
}
