<?php

declare(strict_types=1);

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class FileResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'path' => $this->path,
            'type' => $this->type,
            'link' => $this->link,
            'preview' => $this->preview,
            'thumbnail' => $this->thumbnail,
            'fileable_type' => $this->fileable_type,
            'fileable_id' => $this->fileable_id,
        ];
    }
}
