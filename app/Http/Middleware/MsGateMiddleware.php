<?php

declare(strict_types=1);

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class MsGateMiddleware
{
    public function handle(Request $request, Closure $next)
    {
        if ($request->getHost() !== config('app.gateway')) {
            return response('', 400);
        }

        return $next($request);
    }
}
