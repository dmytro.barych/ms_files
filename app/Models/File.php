<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    use HasFactory;

    public const TYPES = [self::TYPE_AVATAR, self::TYPE_FILE];
    public const TYPE_AVATAR = 'avatar';
    public const TYPE_FILE = 'file';

    protected $fillable = [
        'title',
        'path',
        'fileable_type',
        'fileable_id',
        'type',
        'thumbnail'
    ];

    public function getLinkAttribute(): string
    {
        return config('image.s3_path').$this->path.$this->title;
    }

    public function getPreviewAttribute(): string
    {
        return config('image.s3_path').$this->path.'preview_'.$this->title;
    }

    public function getThumbnailAttribute(): string
    {
        return config('image.s3_path').$this->path.'thumbnail_'.$this->title;
    }
}
