<?php

declare(strict_types=1);

namespace App\Services;

use App\Actions\FileStoreAction;
use App\Dto\FileStoreDto;
use App\Jobs\FileUploadJob;
use App\Models\File;
use Carbon\Carbon;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class FileService
{
    public function store(UploadedFile $file, array $data): File
    {
        $timestamp = Carbon::now()->timestamp;
        $fileName = $timestamp . '_' . $file->getClientOriginalName();

        $data['path'] = 'images/';
        $data['title'] = $fileName;

        $image = Image::make($file);
        $fileFullPath = config('image.image_storage_path') . $fileName;
        $image = $image->stream();
        Storage::disk('s3')->put($fileFullPath, $image->__toString(), 'public');

        FileUploadJob::dispatch($fileFullPath, $fileName)->onQueue('redis');

        $fileStoreDto = FileStoreDto::fromArray($data);
        $fileStoreAction = app()->make(FileStoreAction::class);

        return $fileStoreAction->handle($fileStoreDto);
    }

    public function deleteFile(string $fileName): bool
    {
        if (Storage::disk('s3')->exists(config('image.image_storage_path') . $fileName)) {
            Storage::disk('s3')->delete(config('image.image_storage_path') . $fileName);
        }
        return true;
    }
}
