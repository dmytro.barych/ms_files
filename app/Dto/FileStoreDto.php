<?php

declare(strict_types=1);

namespace App\Dto;

class FileStoreDto
{
    public string $title;

    public string $path;

    public string $type;

    public string $fileableType;

    public int $fileableId;

    public function __construct(string $title, string $path, string $type, string $fileableType, int $fileableId)
    {
        $this->title = $title;
        $this->path = $path;
        $this->type = $type;
        $this->fileableType = $fileableType;
        $this->fileableId = $fileableId;
    }

    public function toArrayForStore(): array
    {
        return [
            'title' => $this->title,
            'path' => $this->path,
            'type' => $this->type,
            'fileable_type' => $this->fileableType,
            'fileable_id' => $this->fileableId,
        ];
    }

    public static function fromArray(array $data): FileStoreDto
    {
        return new static(
            $data['title'],
            $data['path'],
            $data['type'],
            $data['fileable_type'],
            (int)$data['fileable_id'],
        );
    }
}
