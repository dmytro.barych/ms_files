<?php

declare(strict_types=1);

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class FileUploadJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private string $file;
    private string $fileName;

    public function __construct(string $file, string $fileName)
    {
        $this->file = $file;
        $this->fileName = $fileName;
    }

    public function handle()
    {
        $image = Storage::disk('s3')->get($this->file);

        $imagePreview = Image::make($image)->resize(config('image.preview_w'), config('image.preview_h'));
        $imageThumb = Image::make($image)->resize(config('image.thumbnail_w'), config('image.thumbnail_h'));


        $imagePreview = $imagePreview->stream();
        $imageThumb = $imageThumb->stream();

        //preview
        $preview = config('image.image_storage_path')
            .'preview_'
            . $this->fileName;

        //thumbnail
        $thumbnail = config('image.image_storage_path')
            .'thumbnail_'
            . $this->fileName;

        Storage::disk('s3')->put($preview, $imagePreview->__toString(), 'public');
        Storage::disk('s3')->put($thumbnail, $imageThumb->__toString(), 'public');
    }
}
