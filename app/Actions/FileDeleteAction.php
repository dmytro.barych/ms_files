<?php

declare(strict_types=1);

namespace App\Actions;

use App\Dto\FileStoreDto;
use App\Exceptions\BasicException;
use App\Models\File;
use App\Services\FileService;
use Illuminate\Support\Facades\DB;

class FileDeleteAction
{
    public function handle(File $file): ?bool
    {
        try {
            DB::beginTransaction();

            $fileService = app()->make(FileService::class);

            $fileService->deleteFile($file->title);
            $fileService->deleteFile($file->thumbnail);

            $file->delete();

            DB::commit();

            return true;
        } catch (BasicException $exception) {
            DB::rollBack();
        }
    }
}
