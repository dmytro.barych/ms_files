<?php

declare(strict_types=1);

namespace App\Actions;

use App\Dto\FileStoreDto;
use App\Exceptions\BasicException;
use App\Models\File;
use App\Services\FileService;
use Illuminate\Support\Facades\DB;

class FileStoreAction
{
    public function handle(FileStoreDto $dto): ?File
    {
        try {
            DB::beginTransaction();

            $fileService = app()->make(FileService::class);
            $file = File::firstOrNew([
                'fileable_type' =>  $dto->fileableType,
                'fileable_id'=> $dto->fileableId
            ]);

            if ($file->title) {
                $fileService->deleteFile($file->title);
                $fileService->deleteFile('preview_'.$file->title);
                $fileService->deleteFile('thumbnail_'.$file->title);
            }

            $file->fill($dto->toArrayForStore());
            $file->save();

            DB::commit();

            return $file;
        } catch (BasicException $exception) {
            DB::rollBack();
        }
    }
}
